A1k Simple Midi-Out

Midi Interface for use at the serial Port of an Amiga Computer

![Working](Pictures/assembled.jpg)


Interface in action: https://mytube.madzel.de/w/ad2Mzse9jtvgz1xoyjoCgN

Original source: https://gitlab.com/marceljaehne/a1k-simple-midi-out

